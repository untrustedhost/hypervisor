#!/usr/bin/env python3

from __future__ import print_function
import libvirt
import sys
import time
import argparse
import re

parser = argparse.ArgumentParser(description='Start some VMs.')
parser.add_argument('verb',help='invocation mode',choices=['start','status'])
parser.add_argument('--config',help='path to vm start list',default='/etc/untrustedhost/vmstart/default.list')
args = parser.parse_args()

vm_start_list = [] # The list of VMs to start in the given order
vm_start_waiting_time_list = [] # A list of waiting times to wait after each VM reaches active
                                     # state, before starting the next VM from the vm_start_list
try:
  with open(args.config, "r") as config:
    for line in config:
      if line.strip() and not line.lstrip().startswith('#'):
        line_list = line.split()
        vm_start_list.append(line_list[0])
        try:
          vm_start_waiting_time_list.append(int(line_list[1]))
        except IndexError:
          vm_start_waiting_time_list.append(0)
except FileNotFoundError:
  pass

#----------------------------------------------------------------------
def start_vms(libvirt_conn):
    allDomains = libvirt_conn.listAllDomains()
    for i in range(len(vm_start_list)):
      for cfgDomain in allDomains:
        if re.match(vm_start_list[i],cfgDomain.name()):
          if cfgDomain.isActive():
            print("VM '{}' already started".format(cfgDomain.name()))
          while not cfgDomain.isActive():
            print("Starting VM '{}'".format(cfgDomain.name()))
            cfgDomain.create()
            time.sleep(1)
          if cfgDomain.isActive():
            try:
              print("Running delay of {} seconds before starting next VM '{}'".format(vm_start_waiting_time_list[i], vm_start_list[i + 1]))
              time.sleep(vm_start_waiting_time_list[i])
            except IndexError:
              break
    print("All VMs have been started.")


#----------------------------------------------------------------------
def vm_status(libvirt_conn):
    allDomains = libvirt_conn.listAllDomains()

    if len(allDomains) != 0:
      for cfgDomain in allDomains:
        would_start=0
        for ent in vm_start_list:
            if re.match(ent,cfgDomain.name()):
                would_start=1
        print(cfgDomain.name(),would_start,cfgDomain.isActive())

if __name__ == '__main__':
  #connect to hypervisor running on localhost
  conn = libvirt.open('qemu:///system')

  if args.verb == 'start':
      start_vms(conn)
  elif args.verb == 'status':
      vm_status(conn)

  conn.close()

exit(0)
