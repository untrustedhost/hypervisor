#!/usr/bin/env bash

set -e

# create the default storage pool if it's not there.
virsh pool-dumpxml default > /dev/null 2>&1 || {
  virsh pool-define-as default dir - - - - "/var/lib/libvirt/images"
}
