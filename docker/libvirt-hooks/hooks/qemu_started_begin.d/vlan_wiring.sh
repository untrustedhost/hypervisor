#!/usr/bin/env bash

set -x

# primarily: read the <untrustedhost:vlan> definition and configure pvids or names
# found in the XML metadata. as a fallback...
# read the `target` attribute of network bridges and map that to a vlan in the hypervisor
# virt-install ex: --network bridge=b,model=virtio,target=<sysname>__<vlan id or name>
# you need to keep sysname here because target is the name of the tuntap for the guest!

# we are handed a domain xml on stdin
domxml="$(cat)"

lookup_tag() {
  local qs res files single
  files=('/etc/libvirt/hooks/conf/vlans.xml' '/run/untrustedhost/libvirt-conf/vlans.xml')
  qs='vlan/map[@name="'"${1}"'"]/@id'
  res=''
  for single in "${files[@]}" ; do
    res="$(xmlstarlet sel -t -v "${qs}" "${single}")"
    [[ "${res}" ]] && { echo "${res}" ; return 0 ; }
  done
  return 1
}

# walk bridges by mac address and set up the pvids
for m in $(echo "${domxml}" | xmlstarlet sel -t -v '/domain/devices/interface[@type="bridge"]/mac/@address') ; do
  def_mdata='' ; pvid='' ; qs='' ; iftarget='' ; scratch='' ; match=''
  # iftarget may be a named device, may be the transient device (vnetX)
  iftarget="$(echo "${domxml}" | xmlstarlet sel -t -v '/domain/devices/interface[@type="bridge"][mac/@address="'"${m}"'"]/target/@dev')"
  # check for defined pvid
  pvid="$(echo "${domxml}" | xmlstarlet sel -N x='https://untrusted.host/xml' -t -v '/domain/metadata/x:vlan/bridge[@mac="'"${m}"'"]/@pvid')"
  [[ "${pvid}" ]] || def_mdata="$(echo "${domxml}" | xmlstarlet sel -N x='https://untrusted.host/xml' -t -v '/domain/metadata/x:vlan/bridge[@mac="'"${m}"'"]/@vlan')"
  [[ "${def_mdata}" ]] && {
    # we must be able to look up the vlan label at this point or stop the vm.
    pvid="$(lookup_tag "${def_mdata}")"
    [[ "${pvid}" ]] || { printf 'failed to get lookup pvid for label %s\n' "${def_mdata}" 1>&2 ; exit 1 ; }
  }

  # check for embedded pvid
  [[ "${pvid}" ]] || {
    [[ "${iftarget}" ]] && {
      scratch="${iftarget##*__}"
      # did we get scratch back?
      [[ "${scratch}" == "${iftarget}" ]] && continue

      [[ "${scratch}" ]] && {
        # is it all numbers?
        shopt -s extglob
        [[ "${scratch//+([0-9])}" ]] || pvid="${scratch}"
        shopt -u extglob
      }
      # try to look it up then
      [[ "${pvid}" ]] || pvid="$(lookup_tag "${scratch}")"
      [[ "${pvid}" ]] || { printf 'failed to get lookup pvid for label %s\n' "${scratch}" 1>&2 ; exit 1 ; }
    }
  }

  # at this point, if pvid is not set, we can gracefully exit...
  [[ "${pvid}" ]] && {
    # find the device and set the pvid - iftarget may be set already
    bridge vlan del vid 1 dev "${iftarget}"
    bridge vlan add vid "${pvid}" dev "${iftarget}" pvid untagged
  }
done

exit 0
