#!/usr/bin/env bash

confdest="/run/untrustedhost/avahi.conf"
# build an avahi config file if we have any interfaces which should use it.

avahict="$(xmlstarlet sel -t -v 'count(//avahi)' "${IMD_PATH}")"

avahi_ifs=()

while [[ "${avahict}" -ge 1 ]] ; do
  aq="${avahict}"
  ((avahict--))
  vlname=""
  brname=""
  # what *kind* of interface is this?
  # FIXME: we (xmlstarlet) don't deal well with <avahi/> tags at two different levels
  case "$(xmlstarlet sel -t -v "name(//avahi[${aq}]/..)" "${IMD_PATH}")" in
    vlan)
      # grab the vlan name then
      vlname="$(xmlstarlet sel -t -v "//avahi[${aq}]/../@name" "${IMD_PATH}")"
      brname="$(xmlstarlet sel -t -v "//avahi[${aq}]/../../source/@bridge" "${IMD_PATH}")"
      tbname="$(xmlstarlet sel -t -v "//avahi[${aq}]/../../bridge/@name" "${IMD_PATH}")"
      # if we have a target bridge, silently use _that_ name
      [[ "${tbname}" ]] && brname="${tbname}"
      [[ "${vlname}" ]] || { printf 'could not determine vlan name for avahi tag %s\n' "${aq}" 1>&2 ; continue ; } 
      [[ "${brname}" ]] || { printf 'could not determine bridge name for avahi tag %s\n' "${aq}" 1>&2 ; continue ; } 
      avahi_ifs=("${avahi_ifs[@]}" "${brname}-vl-${vlname}")
    ;;
    interface)
      avahi_ifs=("${avahi_ifs[@]}" "$(xmlstarlet sel -t -v "//avahi[${aq}]/../source/@bridge" "${IMD_PATH}")")
    ;;
  esac
done

# only generate a config if we had interfaces to incorporate
[[ "${#avahi_ifs[*]}" -gt 0 ]] && {
  iflist="${avahi_ifs[*]/ /,/}"
  iflist="${iflist%,}"
  printf '[%s]\n' 'server'
  printf '%s=yes\n' 'use-ipv4' 'use-ipv6' 'enable-dbus'
  printf 'ratelimit-interval-usec=%s\n' '1000000'
  printf 'ratelimit-burst=%s\n' '1000'
  printf 'allow-interfaces=%s\n' "${iflist}"

  printf '[%s]\n' 'wide-area'
  printf 'enable-wide-area=%s\n' 'yes'

  printf '[%s]\n' 'publish'
  printf '%s=no\n' 'publish-hinfo' 'publish-workstation' 'publish-domain' \
    'publish-resolv-conf-dns-servers' 'publish-aaaa-on-ipv4' 'publish-a-on-ipv6'

  printf '[%s]\n' 'reflector'
  printf '%s=no\n' 'enable-reflector' 'reflect-ipv'

  printf '[%s]\n' 'rlimits'
  printf '%s=0\n' 'rlimit-core' 'rlimit-fsize'
  printf 'rlimit-nofile=%s\n' '20'
  printf 'rlimit-nproc=%s\n' '4'
} > "${confdest}"
