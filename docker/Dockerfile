FROM 49c67464-20da-4a2c-88a2-9b3cd6f0694c AS mstp-build
RUN /usr/lib/untrustedhost/scripts/pkginst.sh build-essential devscripts \
    debhelper dh-autoreconf
RUN mkdir -p /usr/src/mstpd
ADD /vendor/mstpd.tar /usr/src
ADD /vendor/mstpd-deb.tar /usr/src
ADD /mstpd-pkg.diff /usr/src
RUN cd /usr/src/mstpd && patch --verbose -p1 < ../mstpd-pkg.diff && debuild -b -uc -us
RUN cat /usr/src/mstpd/debian/control
RUN cd /usr/src && tar cf /mstpd-debs.tar mstpd?*

FROM 49c67464-20da-4a2c-88a2-9b3cd6f0694c

# install packages, configure them immediately
RUN /usr/lib/untrustedhost/scripts/pkginst.sh qemu-kvm virtinst \
            libvirt-daemon libvirt-daemon-system libvirt-clients netcat-openbsd firewalld dnsmasq ovmf xmlstarlet \
            tmux scdaemon python3-pip python-setuptools qemu-utils \
            lockfile-progs ipcalc uptimed ntp swtpm swtpm-tools \
            avahi-daemon avahi-utils libnss-mdns nut-client nut-server
COPY --from=mstp-build /mstpd-debs.tar /mstpd-debs.tar
RUN mkdir /tmp/mstpd-x && cd /tmp/mstpd-x && tar xf /mstpd-debs.tar && dpkg -i mstpd*.deb && \
    cd / && rm -rf /tmp/mstpd-x /mstpd-debs.tar

# disable stock ntp, avahi startup
RUN systemctl disable ntp.service
RUN ln -sf /run/untrustedhost/ntp/ntpd.conf /etc/ntpd.conf # seriously?!

# firewalld *needs* it like this to work, tho
RUN firewall-offline-cmd --zone public --add-service mdns

# we don't use the libvirt default network
RUN rm -f /etc/libvirt/qemu/networks/autostart/default.xml

# we do use VMM - configure it now. port 3493 is UPS monitoring.
ADD /systemd-network /etc/systemd/network
RUN find /etc/systemd/network -type f -exec chmod a+r {} \;
RUN firewall-offline-cmd --zone public --add-service ntp

# reconfigure dnsmasq for VMM use
RUN ln -sf /lib/systemd/system/dnsmasq.service "/etc/systemd/system/multi-user.target.wants/dnsmasq.service"
ADD /dnsmasq.conf /etc/dnsmasq.conf

# add md_server
RUN useradd -r -d /var/lib/mdserver mdserver
RUN mkdir -p /usr/lib/untrustedhost/md_server/lib/python3.8/site-packages/
ADD /vendor/md_server.tar /usr/lib/untrustedhost/md_server/src
RUN grep -v ^libvirt-python /usr/lib/untrustedhost/md_server/src/requirements.txt | \
     pip3 install --target=/usr/lib/untrustedhost/md_server -r /dev/stdin && \
    ( cd /usr/lib/untrustedhost/md_server/src && \
      PYTHONPATH=/usr/lib/untrustedhost/md_server/lib/python3.8/site-packages \
       python3 setup.py install --prefix /usr/lib/untrustedhost/md_server )
ADD /mdserver /etc/mdserver

# add VMM/bridging nftables config
ADD vmm.nftables /etc/untrustedhost/vmm.nftables

# add systemd system drop-in files
ADD /systemd-system /etc/systemd/system
RUN find /etc/systemd/system -type f -exec chmod 0644 {} \; && \
    systemctl enable libvirt-default-storage.service && \
    systemctl enable vm-ordered-startup.service && \
    systemctl enable untrustedhost-vmm-ntp.service

# remove the chrony/imd server
RUN rm -f /usr/lib/untrustedhost/imd/chrony

# add sysctl drop-in files (disable ipv6 by default)
RUN mkdir /etc/sysctl.d
ADD /sysctl.d/* /etc/sysctl.d

# add modprobe drop-in files
ADD /modprobe.d/* /etc/modprobe.d

# add apparmor locals
ADD /apparmor.d-local/* /etc/apparmor.d/local

# add tmpfiles.d/tmpfiles-factory
ADD /tmpfiles.d/* /etc/tmpfiles.d
ADD /tmpfiles-factory/* /usr/lib/untrustedhost/tmpfiles-factory

# add libvirt hooks
ADD libvirt-hooks/hooks /etc/libvirt/hooks
RUN mkdir -p /var/log/libvirt/hooks

# logrotate configs
ADD logrotate.d /etc/logrotate.d

# dracut-modules
ADD dracut-modules.d /usr/lib/dracut/modules.d

# configure imd
RUN printf 'set /files/etc/untrustedhost/imd.conf/%s %s\n' \
  'mount_source' '""' \
  'mount_dest' '/etc/untrustedhost' \
  'ssh_user' 'root' \
 | augtool -s -t 'Shellvars incl /etc/untrustedhost/imd.conf'

# add hypervisor imd hooks
ADD untrustedhost-imd /usr/lib/untrustedhost/imd

# add hack for openbsd guests (used via overriding the libvirt emulator)
ADD obsd.qemu-system-x86_64 /usr/local/bin/obsd.qemu-system-x86_64
RUN chmod 0755 /usr/local/bin/obsd.qemu-system-x86_64

# add other scripts
ADD scripts /usr/lib/untrustedhost/scripts
RUN chmod 0755 /usr/lib/untrustedhost/scripts/*

# add build stamps
ADD facts.d /usr/lib/untrustedhost/facts.d
RUN chmod 0644 /usr/lib/untrustedhost/facts.d/*
