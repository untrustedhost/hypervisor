#!/usr/bin/env bash

set -e

# resolve desired imageref and call it bootable:latest

[ "${BASE_IMAGE_REFERENCE:-}" ] || BASE_IMAGE_REFERENCE="registry.gitlab.com/untrustedhost/bootable:jammy"

__warn_msg () { echo "${@}" 1>&2 ; }

# get checkout directory git revision information
__get_cr () {
  local cr
  # initial shortrev
  cr="$(git rev-parse --short HEAD)"
  # check for uncommitted files
  { git diff-index --quiet --cached HEAD -- &&
    # check for extra files
    git diff-files --quiet ; } || cr="${cr}-DIRTY"
  echo "${cr}"
}

# derive build stamps here
[ -n "${CODEBASE}" ] || { __warn_msg "CODEBASE not set" ; exit 1 ; }
CODEREV="$(__get_cr)"
TIMESTAMP="$(date +%s)"

case "${CODEREV}" in
  *-DIRTY) __warn_msg "WARNING: git tree is dirty, sleeping 5 seconds for running confirmation."
           sleep 5
           ;;
esac

echo "building ${CODEREV} at ${TIMESTAMP}"

{
  echo "${CODEBASE}_image_coderev=${CODEREV}"
  echo "${CODEBASE}_image_timestamp=${TIMESTAMP}"
} | tee "docker/facts.d/${CODEBASE}.txt"

# update submodules
git submodule update --init

# copy vendor over
( cd vendor/md_server && git archive --format=tar 0.6.0 ) > docker/vendor/md_server.tar
( cd vendor/mstpd && git archive --prefix=mstpd/ --format=tar HEAD ) > docker/vendor/mstpd.tar
( cd vendor/mstpd-debian && git archive --prefix=mstpd/ --format=tar HEAD debian ) > docker/vendor/mstpd-deb.tar

docker_output_lines="$(docker images "${BASE_IMAGE_REFERENCE}"|wc -l)"
[[ "${docker_output_lines}" -ne 2 ]] && docker pull "${BASE_IMAGE_REFERENCE}"
docker tag  "${BASE_IMAGE_REFERENCE}" 49c67464-20da-4a2c-88a2-9b3cd6f0694c

docker build -t build/release docker
